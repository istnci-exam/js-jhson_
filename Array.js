//4강

//4.1 Arrays
let newYearsResolutions = ['Keep a journal', 'Take a falconry class', 'Learn to juggle'];

console.log(newYearsResolutions);


//4.2 Create an Array
const hobbies = ['ski', 'reading', 'writing'];

console.log(hobbies);


//4.3 Update Elements
const famousSayings = ['Fortune favors the brave.', 'A joke is a very serious thing.', 'Where there is love there is life.'];
const listItem= famousSayings[0];
console.log(listItem);
console.log(famousSayings[2]);
console.log(famousSayings[3]);


//4.4 Arrays with let and const
let condiments = ['Ketchup', 'Mustard', 'Soy Sauce', 'Sriracha'];
const utensils = ['Fork', 'Knife', 'Chopsticks', 'Spork'];

condiments[0] = 'Mayo';
console.log(condiments);

 condiments = ['Mayo'];
console.log(condiments);


utensils[3] = 'Spoon';
console.log(utensils);


//4.5 The .length property
const objectives = ['Learn a new languages', 'Read 52 books', 'Run a marathon'];

console.log(objectives.length);


//4.6 The .push() Method
const chores = ['wash dishes', 'do laundry', 'take out trash'];

chores.push('clean the floor', 'wipe the windows');
console.log(chores);


//4.7 The .pop() Method
const chores = ['wash dishes', 'do laundry', 'take out trash', 'cook dinner', 'mop floor'];

chores.pop();
console.log(chores);


//4.8 More Array Methods
const groceryList = ['orange juice', 'bananas', 'coffee beans', 'brown rice', 'pasta', 'coconut oil', 'plantains'];

groceryList.shift();
console.log(groceryList);


groceryList.unshift('popcorn');
console.log(groceryList);

console.log(groceryList.slice(1, 4)); //index로 구분
console.log(groceryList);

const pastaIndex = groceryList.indexOf('pasta');
console.log(pastaIndex);




//4.9 Arrays and Fucntions
const concept = ['arrays', 'can', 'be', 'mutated'];

function changeArr(arr){
  arr[3] = 'MUTATED';
}

changeArr(concept);

console.log(concept);

function removeElement(newArr){
  newArr.pop();
}
removeElement(concept);
console.log(concept);


//4.10 Nested Arrays
const numberClusters = [[1,2], [3,4], [5,6]];
const target = numberClusters[2][1]; //6