//5강 Loops

//5.1 Repeating Tasks Manually
let vacationSpots = ['Seoul', 'Daegu', 'Busan'];
console.log(vacationSpots[0]);
console.log(vacationSpots[1]);
console.log(vacationSpots[2]);

//5.2 The for Loop
for (let counter = 5; counter < 11; counter++) {
  console.log(counter);
}


//5.3 Looping in Reverse
for (let counter = 3; counter >= 0; counter--){
  console.log(counter);
}

//5.4 Looping through Arrays
const vacationSpots = ['Bali', 'Paris', 'Tulum'];

for (let i = 0; i < vacationSpots.length; i++){
  console.log('I would love to visit '+ vacationSpots[i]);
}


//5.5 Nested Loops
let bobsFollowers =["a","b","c","d"];
let tinasFollowers = ["a","b","e"];
let mutualFollowers =[];
for (let i = 0; i < bobsFollowers.length; i++) {
  for (let j = 0; j < tinasFollowers.length; j++) {
    if (bobsFollowers[i] === tinasFollowers[j]) {
      mutualFollowers.push(bobsFollowers[i]);
    }
  }
};


//5.6 The While Loop
let currentCard = [];
while (currentCard != 'spade') {
currentCard = cards[Math.floor(Math.random() * 4)];
console.log(currentCard)
}


//5.7 Do...While Statements
let cupsOfSugarNeeded = 1;
let cupsAdded  = 0;

do {
 cupsAdded++
 console.log(cupsAdded + ' cup was added') 
} while (cupsAdded < cupsOfSugarNeeded);


//5.8 The break Keyword
const rapperArray = ["Lil' Kim", "Jay-Z", "Notorious B.I.G.", "Tupac"];

// Write your code below
for (let i = 0; i < rapperArray.length; i++) {
  console.log(rapperArray[i]);
  if (rapperArray[i] === 'Notorious B.I.G.'){
    break;
  }
}
console.log("And if you don't know, now you know.");