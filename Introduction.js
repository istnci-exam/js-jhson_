//1강 Introduction

//1.1 introduction
//1.2 console
console.log(5);
console.log(10);

//1.3 comments
//opening line
/* mulitline comments
console.log('It was love at first sight.');
console.log('The first time Yossarian saw the chaplain he fell madly in love with him.');
console.log('Yossarian was in the hospital with a pain in his liver that fell just short of being jaundice.');
console.log('The doctors were puzzled by the fact that it wasn\'t quite jaundice.');
console.log('If it became jaundice they could treat it.');
console.log('If it didn\'t become jaundice and went away they could discharge him.');
console.log('But this just being short of jaundice all the time confused them.');
*/

//1.4 data type
console.log('JavaScript');
console.log(2011);
console.log('Woohoo! I love to code! #codecademy')
console.log(20.49);

//1.5 Arithmetic Operatior
console.log(3.5 + 27);
console.log(2021 - 1969);
console.log(65/240);
console.log(0.2708 * 100);

//1.6 String Concatenation
console.log('Hello' + 'World');
console.log('Hello' + ' ' + 'World')


//1.7 Properties
console.log('Teaching the world how to code'.length); //30

//1.8 Methods
// Use .toUpperCase() to log 'Codecademy' in all uppercase letters
console.log('Codecademy'.toUpperCase());//CODECADEMY
// Use a string method to log the following string without whitespace at the beginning and end of it.
console.log('    Remove whitespace   '.trim());//Remove whitespace

//1.9 Built-in Objects
console.log(Math.floor(Math.random()*100));
Math.floor(Math.random() * 100);
console.log(Math.ceil(43.8));
console.log(Number.isInteger(2017));

//2강 Variables
//2.1 Create a variable : var
var favoriteFood = 'pizza';
var numOfSlices = 8;
console.log(favoriteFood);
console.log(numOfSlices);

//2.2 Create a Variable : let
let changeMe = true;
changeMe = false;
console.log(changeMe);

//2.3 Create a Variable : const
const entree = 'Enchiladas';
console.log(entree);
//entree = 'Tacos'; //error

//2.4 Mathematical Assignment Operators
let levelUp = 10;
let powerLevel = 9001;
let multiplyMe = 32;
let quarterMe = 1152;
// Use the mathematical assignments in the space below:
levelUp += 5;
powerLevel -= 100;
multiplyMe *= 11;
quarterMe /= 4;
// These console.log() statements below will help you check the values of the variables.
// You do not need to edit these statements. 
console.log('The value of levelUp:', levelUp); 
console.log('The value of powerLevel:', powerLevel); 
console.log('The value of multiplyMe:', multiplyMe); 
console.log('The value of quarterMe:', quarterMe);

//2.5  The Increment and Decrement Operator
let gainedDollar = 3;
let lostDollar = 50;

gainedDollar++;
lostDollar--;

//2.6 String Concatenation with Variables
let favoriteAnimal = 'Elephant';
console.log('My favorite animal: ' + favoriteAnimal);

//2.7 String Interpolation
const myName = "JHSON";
let myCity = "Daegu";
console.log(`My name is ${myName}. My favorite city is ${myCity}.`);

//2.8 typeof operator
let newVariable = 'Playing around with typeof.';
console.log(typeof newVariable);

newVariable = 1;
console.log(typeof newVariable);