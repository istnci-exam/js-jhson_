//3강 Functions

//3.2 Fuction Declarations
function getReminder(){
    console.log('Water the plants.');
  }
  
  function greetInSpanish(){
  console.log('Buenas Tardes.');
  }
  greetInSpanish();


//3.3 Calling a Function
function sayThanks(){
  console.log('Thank you for your purchase! We appreciate your business.');
}

sayThanks();
sayThanks();
sayThanks();


//3.4 Parameters and Arguments
function sayThanks(name) {
    console.log('Thank you for your purchase '+ name + '! We appreciate your business.');
  }
  
  sayThanks('Cole');


//3.5 Default Parameters
function makeShoppingList(item1 = 'milk', item2 = 'bread', item3 = 'eggs'){
  console.log(`Remember to buy ${item1}`);
  console.log(`Remember to buy ${item2}`);
  console.log(`Remember to buy ${item3}`);
}

makeShoppingList();


//3.6 Return
function monitorCount(rows, columns){
    return rows * columns;
    }
    
const numOfMonitors = monitorCount(5, 4);
console.log(numOfMonitors);


//3.7 Helper Functions
function monitorCount(rows, columns) {
    return rows * columns;
  }
  
  function costOfMonitors(rows, columns){
    return monitorCount(rows, columns) * 200
  }
  
  const totalCost = costOfMonitors( 5, 4);
  console.log(totalCost);


//3.8 Function Expressions
const plantNeedsWater = function palntNeedsWater(day){
    if(day === 'Wednesday'){
       return true;
     } else {
       return false;
     }
   }
   
   plantNeedsWater('Tuesday');
   console.log(plantNeedsWater('Tuesday'));


//3.9 Arrow Functions
const plantNeedsWater = (day) => {
    if (day === 'Wednesday') {
      return true;
    } else {
      return false;
    }
  };
  

//3.10 Concise Body Arrow Functions
const plantNeedsWater = (day) => {
    return day === 'Wednesday' ? true : false;
  };
const plantNeedsWater = day => day === 'Wednesday' ? true : false;
